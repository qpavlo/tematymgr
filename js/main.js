var topics = new Firebase('https://tematy-magisterskie.firebaseio.com/topics');
var topic = {};

topics.on('child_added', function (snapshot) {
    var topic = snapshot.val();
    var topicID = snapshot.name();
    $('#topics-table').prepend(
        '<tr id="' + topicID + '">' +
            '<td>' + topic.topic + '</td>' +
            '<td>' + topic.promoter + '</td>' +
            '</tr>');
});

topics.on('child_removed', function (snapshot) {
    var topicId = snapshot.name();
    $('#topics-table').find('#' + topicId).remove();
});

$('#topic-form').submit(function (event) {
    var topicInput = $("#topic");
    topic.topic = topicInput.val();
    nextStep();
    topicInput.val("");
    event.preventDefault();
});

$('#promoter-form').submit(function (event) {
    var promoterInput = $("#promoter");
    topic.promoter = promoterInput.val();
    nextStep();
    promoterInput.val("");
    event.preventDefault();
});

function nextStep() {
    $(".step.active").fadeOut().removeClass('active').next(".step").fadeIn().addClass('active');
}